#! TITLE: Buster Freeze Policy
#! SUBTITLE: let's release!

What happens when we freeze
---------------------------

We have some criteria for what changes we are going to accept.  These
are <a href="#guidelines-changes">listed below</a>.  These criteria
will become more rigid as the freeze progresses.

We will continue to do auto-removals during the entire freeze.  Please
see <a href="#autoremovals">below for more details</a>.

The release managers may make exceptions to these guidelines as they
see fit. **Such exceptions are not precedents and you should not
assume that your package has a similar exception.** Please talk to us
if you need guidance.

Please talk to us early and do not leave issues to the last minute. We
are happy to advise in case you need the release team's help to fix RC
bugs (e.g.  to remove an old package)


Changes which can be considered
-------------------------------


 1. targeted fixes for release critical bugs (i.e., bugs of severity critical, grave, and serious) in all packages;
 1. fixes for severity: important bugs in packages of priority: optional or extra, only when this can be done via unstable;
 1. translation updates and documentation fixes that are included with fixes for the above criteria;

Note that when considering a request for an unblock, the changes
between the (proposed) new version of the package in `unstable` and
the version currently in `testing` are taken in to account. If there
is already a delta between the package in `unstable` and `testing`,
the relevant changes are all of those between `testing` and the new
package, not just the incremental changes from the previous `unstable`
upload. This is also the case for changes that were already in
`unstable` at the time of the freeze, but didn't migrate at that
point.

Applying for an unblock
-----------------------

 1. Prepare a **source** `debdiff` between the version in `testing` and `unstable` and check it carefully
 1. Use `reportbug` to report an unblock bug against the `release.debian.org` meta-package. Attach the source diff. Include a detailed justification of the changes and references to bug numbers.
 1. If the diff is small and you believe it will be approved, you can upload it before filing the unblock request to avoid a round-trip.
 1. Depending on the queue, there may be some delay before you receive further instructions.

If you are unable to bring your fix through unstable, for example
because there are unrelated changes already uploaded there, the
release team can grant you permission to use the
`testing-proposed-updates` mechanism. Prepare an upload targeting
`testing-proposed-updates` but **do not upload it**, and then contact
us through an unblock bug.

Targeted fixes
--------------

A targeted fix is one with only the minimum necessary changes to
resolve a bug. The freeze process is designed to make as few changes
as possible to the forthcoming release. Uploading unrelated changes is
likely to result in a request for you to revert them if you want an
unblock.

Some examples of changes that are undesirable during a freeze:

 1. dropping a -dbg package in favour of -dbgsym
 1. adding a new systemd unit in place of an init script

Removing packages from testing during the freeze
------------------------------------------------

Throughout the freeze, we will continue to remove non-key
packages with RC bugs in testing and their reverse dependencies automatically.
As usual, the auto-removal system will send a notice before this happens.
Please note that it is not enough for the bug to be fixed in unstable.
The fix (in unstable or testing-proposed-updates) must be done in such a way that
the fixed package can be unblocked and allowed to migrate to testing, based on the rules
listed above. **You must contact us to ensure the fix is unblocked - do not rely on
the release team to notice on their own.**

Manual removal may still happen without warning before the standard auto-removal
periods have passed, when the package is blocking other fixes.

After 12th February 2019, removed packages will **not** be permitted to re-enter testing.
