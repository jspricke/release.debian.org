Hi.

During the Bug Squashing Party happening last weekend the release team
also hinted  a lot of packages for removal from testing. Since this is
something that can happen to all maintainers at any point of the release
process, we want to refresh why and how testing removals happen.
(Attached to the mail you can also find a list of all packages removed
from testing during the weekend)

Please note that due to the transition to gcc 4.0 a lot of old bugs
were upgraded to an RC severity lately, a fact which could have easily
slip your attention. This is probably a good time to check your package
overview (http://qa.debian.org/developer.php) to see if some of your
packages have RC bugs and/or were removed from testing.


Q: Why do you remove packages from testing?

A: Because we can't fix them all. While the release team feels certainly
responsible to bring Debian as a whole in a releasable state we cannot
fix each individual package. This is the maintainer's responsibility.
If a package is not in a releasable state and if there aren't any reasons
to give it special attention (e.g. it has a lot of packages depending on
it) then it should not be part of testing.


Q: When do you remove packages from testing?

A: We remove packages when they are not in a releasable state, i.e. when
they have RC bugs (that affect the version in testing). Of course it
doesn't make sense to immediately remove each and every package that
gets an RC bug therefor we usually ignore "new" bugs. What we define
as "new" depends on where we are in the release process, shortly
before sarge release we usually used a week, currently this is more
like three to four weeks.

There are some factors that can hinder us from removing a package, though:
 - if it is essential (obviously ;)
 - if it has a lot of reverse (build-)dependencies

And there are some factors that let us usually refrain from removing a
package:
 - if the maintainer can't do anything about the RC bug(s), because he
   has to wait on other packages (e.g. all packages that depend on Qt
   currently)
 - if the maintainer is obviously actively working on fixing the bug
   and just needs some more time
 - if the package has a really large user base and we fear the
   negative effects on users of the package in testing would
   outweight the positive effects on the release

Also, please note that removing packages from testing happens on a
case-by-case-basis, means: it happens that some packages that meet
our criteria for removal from testing are already removed, while
others are not.


Q: My package was removed from testing, what should I do?

A: Fix the RC bugs. At the current point in the release process there should
be enough time for any package to get back to testing in time.
(If the freeze gets nearer and you have fixed your RC bugs and the
package still can't get back to testing because of reasons you don't have
any influence on, please contact debian-release@lists.debian.org to
make us aware of the issue)


Q: How can I keep my package from being removed from testing?

A: Fix your RC bugs, fix them fast. Remember that as the maintainer,
you are the person responsible for keeping the package in a releasable
state. If you find yourself unable to fix a release critical bug in
your package, then

 - document the reasons you can't fix the bug in the bug report
 - tag the bug help and ask for patches and/or NMUs
 - get some co-maintainers

This doesn't guarantee that the package won't be removed from testing,
but it is the best way to ensure that a *fixed* package enters testing 
quickly.


Q: You removed my package from testing without notifying me. Why?

A: Because we simply don't have the time. Please make sure you get
notified when one of your packages gets an RC bug and try to investigate
them as fast as possible. If you can't fix them right away document that
in the bug report. Document anything we should know in the bug report!

If you aren't able to ensure that you can react to RC bugs (at least by
mail, not necessarily with an upload) within one or two weeks please
consider finding some co-maintainers or to try to identify some people
you could ask to do an NMU in case you're busy.


Q: But the bug was trivial to fix and/or there was already a patch
available. Why didn't you just NMU the package instead of removing it?

A: Because there are 300 other bugs like yours. Doing a NMU for a trivial
RC bug usually takes 15-30 minutes depending on the package (with the
most time going into testing the result before uploading). Multiply
that by the number of RC bugs we currently have... Hinting a package
for removal usually takes about one minute (with the most time going
into reading the bug report and checking the reverse dependencies).


Q: How can I find who removed my package and why?

A: As long there is an active removal hint for any version of your package
this information is available on the PTS page of your package
(packages.qa.debian.org) and in the current output of the testing scripts
(see http://www.debian.org/devel/testing for the links). Older hints are
only available in the hints files themself, which are accessible at
http://ftp-master.debian.org/testing/hints/. There you should usually
find a short reason for the removal, too (but in most cases this will just
be the bug numbers of your RC bugs).

--
Frank Lichtenheld <djpig@debian.org>
$Id: removing-packages-from-testing,v 1.11 2005/08/08 23:11:17 djpig Exp $
