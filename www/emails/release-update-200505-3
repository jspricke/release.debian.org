To: debian-devel-announce@lists.debian.org
Subject: Release update: minor delay; no non-RC fixes; upgrade reports

Hi,

Well, just in case it wasn't obvious to everyone from looking at the
release-critical bug stats, we should probably come out and say it:  the
the count of release critical issues affecting sarge is still going
down, but it's not yet down to zero, which means no release this
weekend.

But we are *very* close, so we're only pushing the schedule back a week
and aiming for a release next weekend.

The only real blocker as of today are the release critical bugs.  If you
have any of them in your own package, take care of them now.  You
wouldn't want to be known as The Maintainer Who Held Up The Release, and
you probably don't want your package removed from sarge, either. :)
Otherwise, if you want to help us, please continue to squash RC bugs and
help with the preparation of the release notes and processing of upgrade
reports.  We're at a point now where more hands are not going to speed
up the release, though, so if you aren't already involved in these
tasks, you might want to just relax for a bit and start your Release
Party preparations.


There has been a great deal of interest from maintainers wanting
to get fixes into sarge.  Thank you for helping to make your packages the
best possible for sarge!  However, since we are in a freeze, each fix
requires time from a member of the release team to check the package for
regressions.  Given the pure number of requests, this is a major time sink,
so please make sure that your request matches our criterias before sending
it in.

This means that, for all packages that still need to be updated for
sarge, the rules are (still) as follows:

  - Updates are only possible for RC-bug fixes and translation and
    documentation improvements.

  - If your package needs to be updated for sarge, and the version in
    unstable doesn't contain extraneous changes (e.g, the version is the
    same between testing and unstable), please upload your fix to
    unstable and contact debian-release@lists.debian.org.

  - If the version in unstable already includes significant changes not
    related to the bug to be fixed, contact debian-release about
    uploading to testing-proposed-updates.  Changed dependencies, new
    upstream versions, changed library names, and completely rewriting
    the packaging are "significant changes".  So are lots of other
    things.

  - If the version in unstable won't reach testing because of new
    library dependencies, contact debian-release about uploading to
    testing-proposed-updates.

  - If in doubt, contact debian-release first.

  - In all cases, when preparing an upload please do not make changes to
    the package that are not related to fixing the bugs in question.
    Doing so makes it more time consuming for the release team to review
    and approve such requests, delaying the release.  It also delays the
    fix for your package, because you will be asked to reupload.

  - When contacting the release team, please explain why you are
    requesting an update.  Bug numbers are a must.  The more we can
    figure out from your first email and your changelog (if any), the
    more quickly we can get your update in.

  - If you have a package that needs updating, *please* don't forget to
    contact us.  *Don't expect us to find out about it on our own*.
    Putting a comment in the changelog is not contacting the release
    team. :)  (This has happened at least a couple of times during the
    base freeze; it's not a very good way of getting your package
    approved quickly.)


Also, please remember that we can't promise anything about non-RC-bug
fixes, given the high number of requests that hit us.  And, for releasing
sarge, we just need to say "no" sometimes.  It's way too late for random
changes now; and even asking a member of a release team about such a
change causes work for the release team and takes time away from working
on release blockers.

To summarize again what is acceptable for a freeze exception:

  - fixes for release critical bugs (i.e., bugs of severity critical,
    grave, and serious)

  - translation updates

  - documentation fixes

See <http://lists.debian.org/debian-devel-announce/2004/08/msg00001.html>
and <http://lists.debian.org/debian-devel-announce/2005/05/msg00001.html>
for comparison.

As always, it is the release team's goal to get as much good software
into sarge as possible.  However, a freeze does not mean that your
package is ensured a spot in the release.  Please continue to stay on
top of release-critical bugs in packages that you maintain; RC bugs in
optional or extra packages that remain unfixed after five days will
still be grounds for removal from testing (and as of now, re-introducing
such packages is not possible any more).

Please also note that since many updates (hopefully, the vast majority)
will still be going in through unstable, major changes in unstable right
now can disrupt efforts to get RC bugs fixed.  We don't ask you not to
make changes in unstable, but we do ask that you be aware of the effects
your changes can have -- especially if you maintain a library -- and to
feel free to continue making use of experimental where appropriate.
Note once again that you can stage NEW uploads in experimental to avoid
disruption in unstable.


Timeline
--------

So the remaining (very short) timeline looks like this:

  27 May 2005 (that's today)
  ~50 RC bugs (~30 w/o security)
  Permanent Bug Squashing Party

Since the start of the freeze, there have been a couple of rounds of new
RC bugs filed, the result of which is still being cleaned up.  So, the
official RC bug markers have not gone down as far as wanted, but we are
making great process towards release.  Just a few more bugs to go!

A large fraction of these bugs (ten or so) are bugs about kernel-patch
packages that don't apply to sarge kernels, and should be removed
shortly.  The rest of the bugs also need to be addressed in short order,
by removals or fixes; as well as various behind-the-scenes bugs that are
closed but the fixes have not quite yet reached sarge.

  1 June 2005
  ~15 RC bugs (excluding security bugs)
  0 RC bugs not tagged "sarge"

As before, being able to hold to this schedule depends heavily on a
steadily dropping RC bug count, so if that isn't happening, the timeline
will have to be tweaked accordingly.  Security bugs will, however, not
figure into this count for the most part because they can and will be
fixed post-release.

  3 June 2005
  0 RC bugs

Any remaining release-critical bugs will be fixed through uploads to
testing-proposed-updates or by removals from sarge.

With a final cut of the installer in the bag and the effective RC count
down to zero, it's time to finalize the installation manual and release
notes and to create official CD images.


  6 June 2005
  Release

And a little time passes, the CD team and the FTP team take a few
minutes out of their relaxing weekends to wave their magic wands, and
if the incantation works right, we'll have a shiny new release on
Monday.


We'll continue to post updates as the freeze goes on.  For now, please
concentrate on fixing the last few issues.


Cheers,
-- 
Andi Barth
Debian Release Team
