DRAFT!!!

Subject: Etch in the hands of the Stable Release Managers
To: debian-devel-announce@lists.debian.org

Hi,

You might read about now in debian-announce [1] that we have just
released. Please don't upload all your new software instantly, but
wait a few days until the archive is set up properly for Lenny.

At this point, we would like to thank all people who have helped to
release Debian 4.0 (Etch). Only through the work of the community we
were able to get to this point.

It's hard to write "thank you"s for a project as big as etch because there
are *so many* people involved in pulling this off, but they deserve
recognition for etch as much as we do -- people like the ftp team, the
installer team, the CD team, the mirror team, the porters, the release notes
editors, the translators, and an amazing group of "release minions" who
just worked wherever it was necessary like Steinar Gunderson, all of whose
contributions were essential for getting us to this point.

Special thanks to all those who have worked through the weekend for those
finishing touches that brought the release together, including Ryan, Anthony,
Frans, Steve McIntyre, Frank Lichtenheld, Mattias Wadenstein, Alexander Schmehl,
and many others.

And of course, thanks to the Debian Project as a whole, including
the innumerable mass of people who have provided patches, reported
bugs against testing or contributed in some other way.  This is your release,
friends.

We also want to apologize to the translations teams and thank them for
their work on the localisation of the release notes, our provided
deadlines were quite tough. Thanks for handling this so well.


Preparing the first point-release
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
There are already some packages known that will need an update in stable
soon.  Best known is the kernel, which has some annoying bugs with pending
fixes. If you are aware of any important bug that is waiting
in unstable and didn't make the release because of time constraints,
send a mail to debian-release@lists.debian.org.  The usual rules that
were in place for freeze exceptions are also mainly valid for the first point 
release of the Etch life cycle. It will be possible to accept packages
directly from unstable to stable-proposed-updates. If you want the stable
release team to do that, wait with new uploads until you got a confirmation
that the package was accepted.

Also we might accept bug fixes to meet our release goal of LSB 3.1 compliance.


Continuing with Lenny
~~~~~~~~~~~~~~~~~~~~~
After the release is before the release. We don't have too specific plans
for the Lenny release cycle yet, but there are a couple of things that
are already known:

 * Lenny should be kept in a releaseable state most of the time (with the
   exception of breakages caused by one of the big transitions; but we
   should try to keep even these as small as possible).  This
   means that there are no excuses for not fixing an RC bug.  The problems
   that were uncovered by piuparts runs in the past two weeks have been
   mostly ignored for Etch, but they need to be fixed for Lenny.  These
   are trivial fixes, please apply them and upload soon.

 * Please coordinate your uploads a bit, so that the testing transition
   of packages stays possible.  Everybody uploading new software
   usually makes britney cry and hinders the transition of new versions.

 * We will work with release blockers and release goals again for the
   Lenny release cycle.  If you have anything that you feel belongs in
   one of these two categories, send a mail to -release, so that we can
   include it in our plans.  However, we want you to remember that
   -release is not a discussion list.

 * We want to restrict the release cycle for Lenny to less than 2 years,
   we want to discuss experiences of Etch first though to get a more 
   accurate time planning.


We'll soon send a more in-detail information about what we think we learned
from Etch, and a few more things for Lenny. We'll also try to have an
Lenny-kick-off-meeting soon - we'll announce that in time as well, so that
anybody can feed us with ideas.


Now, we'll party and let the SRMs do the rest until Etch is out.  Thanks
again to all of you for the great support Debian got.


Yours Debian Release Team
Andi, Steve, Luk, Marc
-- 
http://release.debian.org/
[1] http://lists.debian.org/debian-announce/
