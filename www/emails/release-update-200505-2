To: debian-devel-announce@lists.debian.org
Subject: Release update: freeze progress, closing date for non-RC fixes

Hi-ho,

We're now two weeks into the freeze, and the count of release critical
bugs affecting sarge is dwindling.  Unfortunately, due to a number of
RC bugs that were found after the freeze announcement, even though we've
closed out about 50 RC bugs with your help during these past two weeks,
the net count is only down by about 30.

According to <http://bugs.debian.org/release-critical/>, the official
count of release-critical bugs affecting testing is 61.  Since security
bugs are an, er, "renewable resource", and can be fixed out-of-band, we
can exclude them from our reckoning and get the number at the bottom of
<http://bts.turmzimmer.net/details.php?ignore=sid&ignsec=on> instead,
which is 41.  This is pretty good progress, but it's still a far cry
from the estimate of 15 RC bugs that our timeline called for by this
Wednesday.  We fortunately did put a little bit of padding into this
timeline, but being off on the RC count by a factor of 2-3 is stretching
things a bit, y'know?

So, let's make a few changes to try to get us back on track for an
end-of-May release.


First, there has been a great deal of interest from maintainers wanting
to get fixes for severity: important bugs into sarge.  Thank you for
helping to make your packages the best possible for sarge!  However,
since we are in a freeze, each fix for an important bug requires time
from a member of the release team to check the package for regressions.
Since there have been more than 200 requests for freeze exceptions since
the freeze began, this is a sizeable chunk of time that the release team
is spending *not* working on those 41 release critical bugs.  So as of
Thursday, the window is closed for fixes to severity: important bugs and
requests to re-add packages to sarge that weren't in woody.  (We *may*
still accept translation and documentation fixes, and of course all RC
bugs must go.)  If you have outstanding important bugs that you are
hoping to get fixed for sarge, please be sure to contact debian-release
by Thursday.

We can't promise that everything submitted by Thursday will get into
sarge; in fact, we can't even promise that we'll get a chance to review
all the requests, since there is already a backlog.  Sorry, but it's a
choice between spending time on RC issues, and spending time on
important-but-not-RC ones.  For the record, the smaller and more
self-evident the diff, the more likely it is your package will get in.

We have also received requests for freeze exceptions that didn't follow
the guidelines of "important or higher fixes only; no extraneous
changes."  If you sent us mail requesting an exception for your package,
and you haven't gotten a reply yet, make sure you were following the
guidelines in [1].  If you weren't, you probably aren't going to get a
reply, and your package isn't going to get in either.


Second, we need to be making sure bugs get closed at a much better rate
than they're being opened; so as an incentive, let's drop the window for
bug fixes from 7 days to 5.  In other words, if you have a package of
priority: optional or priority: extra with an RC bug that has been open
for 5 days, you can now expect this package to be kicked out of testing.


Third, let's have a BSP this weekend.  BSPs are fun, and they fix bugs,
too!  You can find us in the usual place, in #debian-bugs on
irc.debian.org.

The focus of this weekend's BSP should be to fix RC bugs.  You should
*only* NMU packages for those release-critical issues that affect sarge.
In fact, since a few of our architectures are struggling to keep up with
the current package load in unstable and testing-proposed-updates,
NMUing for anything other than those 41 RC bugs is downright
counterproductive -- so don't do it.  Otherwise, the 0-day NMU policy is
still in effect.  People who are RC-bug-averse can also participate in
the BSP by letting us know how bad (or, well, how good) the upgrade path
from woody to sarge is, by filing upgrade reports[2].

If you NMU packages during the BSP please always send a mail to the
release team, too (debian-release@lists.debian.org). A copy of the
mail with the complete NMU patch you will send to the bug report usually
suffices. Doing so minimizes the risk of us overlooking the upload and
forgetting to include it in sarge. Please *don't* reopen the bug and
tag it 'sarge'. We will use this only for packages for which we can't
approve the current version in unstable.


Wall of shame, or: things not to do during a freeze
---------------------------------------------------
Speaking of "don't do that", it seems that not everyone read and
understood what we meant in the freeze announcement when we said to be
aware of the effects your uploads to unstable can have, so let's
illustrate with a few examples.

Uploading a new revision of a three-year-old library package just to
make it compatible with gcc 4.0: don't do that.  gcc 4.0 isn't even in
*sid* yet, let alone in sarge, so this is, like, release anti-critical.
Especially when your library depends on an arch: all package and your
upload makes it unusable as a build-dep until it's up-to-date on all
archs.

Uploading a new upstream version of a library that's a build-dependency
of xfree86, while a release-critical update of xfree86 is in
preparation, without talking to the xfree86 maintainers first.  Don't do
this.  The *best*-case scenario is that I get to spend an hour or two
sorting out whether this upload has broken the library ABI.  The
worst-case scenario is much, much worse.  When I say you should talk to
the maintainers of packages that depend on yours, I really, really mean
it.

* Trigger a rebuild, because the i386 package wasn't built properly.
  (Closes: Bug#309365)
Yeah, don't do this one either.  Whether we're in a freeze at the time
or not, it is a waste of buildd cycles to do a *sourceful* upload to
correct for a problem with the way *binaries* were built in your build
environment.  If you're not making changes to the source, you probably
shouldn't be uploading a new source package.

Indeed, all of the above are pretty good advice at all times, but during
a freeze these are the kinds of things that can really hurt the release
timeline.


Timeline
--------

Oh yes, the timeline.  Here's (sorta) what it looks like, from today
forward:

  18 May 2005
  ~40 RC bugs
  d-i final needed?  No.

So the RC bug count is higher than we want it to be, but definitely
moving in the right direction.  Can we get it where we want it by the
end of the month?  Possibly, if more people pitch in over the next week
or so...

There has been no further action on preparing architecture-specific
updates for d-i, so there will be no d-i final.  This means that no
outstanding kernel changes will be integrated into sarge until r1.
Fixes for security bugs should be available from security.debian.org
well before then, of course.

  20-22 May 2005
  ~30 RC bugs
  Bug Squashing Party

The count of 30 RC bugs should be the count *before* the BSP; at the
conclusion of the BSP, we want this number to look more like 10.  Please
join us in getting rid of those last few troublesome RC bugs on the way
to the release.

  25 May 2005
  ~10 RC bugs
  0 RC bugs not tagged "sarge"

Since it takes a few days for autobuilders to catch up after a source
package has been uploaded, we want to have fixes uploaded to unstable or
testing-proposed-updates for all RC bugs by the 25th.  The RC bug count
should at this point therefore not include any bugs that aren't tagged
"sarge".

  27 May 2005
  0 RC bugs
  Final version of release notes available

The rest of the RC bug fixes get into testing, or the packages get
removed, or the schedule slips a week.  Take your pick -- I certainly
prefer the first option.

At the same time, we want to have the release notes finished up, as well
as the installation manual if any changes are still pending.  This
leaves very little time for the documentation team to incorporate
feedback from upgrade testing, and even less time for localization teams
to complete translations.  Since there are currently only three
translations of the release notes that are reasonably up-to-date, we
should plan to allow translations to be updated after the release
itself.  They obviously won't get onto the CD images (which we'll start
building at this time), but there were no release notes on the CDs for
woody at all, so it's not a total loss to do things this way.


  30 May 2005
  Release

And if everything goes well, we'll be ready to release at the end of the
month.

If everything *doesn't* go well, then we're hopefully looking at the
first weekend in June instead.


Right now, this schedule is looking more ambitious than when we cooked
it up, but it's not completely out of the question -- we just need to
pick up the pace a bit.

Cheers,
-- 
NN
http://release.debian.org/

[1] http://lists.debian.org/debian-devel-announce/2005/05/msg00001.html
[2] http://lists.debian.org/debian-devel-announce/2005/05/msg00010.html
