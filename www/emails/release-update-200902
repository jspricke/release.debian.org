To: debian-devel-announce@lists.debian.org
Subject: Release Update: deep freeze, planned dates, and remaining bugs

Hello!

Deep freeze
===========

As you've probably read by now, the Installer Team has announced [1] the
availability of the second, and hopefully final, release candidate for
the Lenny installer. Testing of these images is highly encouraged.

Following the plan outlined in the previous release update [2], we are
now in deep freeze, which means that we'll only be migrating to testing
packages that fix RC bugs.

Please don't send other types of unblock requests, unless you assess
that our time hearing your reasons will be time well spent; thanks in
advance. (If you sent a request some time ago, please follow-up to the
original mail as soon as possible, in case it has been forgotten.)

We're also deactivating the "freeze-exception" hint file, which provided
automatic freeze exceptions for package versions that were uploaded prior
to the start of the freeze. A total of 1197 exceptions were automatically
granted, of which 983 managed to migrate in time for Lenny.

  [1]: http://lists.debian.org/debian-devel-announce/2009/01/msg00009.html
  [2]: http://lists.debian.org/debian-devel-announce/2008/12/msg00006.html


Schedule
========

The weekend of February 14th is going to be our tentative target for
release. We've checked with all the involved teams (which are many!),
and the date works for all of them.

The intention is only to lift that date if something really critical
pops up that is not possible to handle as an errata, or if we end up
technically unable to release that weekend (eg., a needed machine
crashes). Every other fix that doesn't make it in time will be r1
material. Please be sure to contact us about the RC fixes you'd like
included in the point release!

(As an example of this, the new archive key [3] did not came in time to
make it into the d-i rc2 images. We assessed the situation, and this
only poses a problem for installing testing/squeeze with the Lenny
installer, and once the new key is in use. So, we'll be updating the
installer in 5.0r1 for this. [NB: the key will be in Lenny in the
debian-archive-keyring package; only in the installer images is missing.])

Also, our intent is that there will be a "Lenny and a half" release
half-way through the Squeeze release cycle, similarly to what was done
for Etch, where additional hardware support will be considered.

  [3]: http://lists.debian.org/debian-devel-announce/2009/01/msg00008.html


Remaining RC bugs
=================

So, as hinted in the previous release update, it's time to run really
fast towards a release. Here's what we're going to propose regarding the
remaining RC bugs.

We'll be working against the list of RC bugs affecting Lenny that are
not fixed in unstable:

    http://bts.turmzimmer.net/details.php?bydist=both&sortby=bugnr&new=7&refresh=1800 

Members of the Release Team will be adding comments to that list, in the
form:

    Lenny: can-defer
    Lenny: will-remove
    Lenny: is-blocker
  
Which mean, respectively, that a fix can be deferred to r1 (we expect
most of the bugs will fall in this category), or the package will be
removed prior to the release (if having it ship with Lenny hurts more
than the package being absent from the release), or that it must
absolutely be fixed.

When we add an "is-blocker" tag, we'll send a mail to -devel as well,
though it is our hope that there won't be any. Also, if you disagree
with any of the labellings, please send your rationale to -release.

Nevertheless, fixes for "can-defer" or "will-remove" bugs will be *very*
welcome. In fact, you are very much encouraged to go over the list
yourself, and apply *your* judgement as to what bugs should be blockers,
and then fix them, or convince somebody to fix them. We'll be doing our
best to ensure all uploaded fixes make it into Lenny r0.

As always, you need not send unblock requests for RC bug fixes, but
you're welcome to if you prefer to receive an explicit ACK that it's
been unblocked.

Finally, if you can't fix RC bugs, maybe you can help with the Release
Notes. A call for help was sent back in November [4], and this is the
last opportunity to provide any patches before the coordinators set a
freeze date for translators to work.

  [4]: http://lists.debian.org/debian-doc/2008/11/msg00034.html
       (Note that since that announcement, the Release Notes have moved
       from /branches/lenny to /trunk.)

Cheers,
