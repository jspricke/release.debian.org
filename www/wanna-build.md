#! TITLE: Requesting wanna-build actions
#! SUBTITLE: How to get rebuilds of your package

Asking for wanna-build actions
==============================

Intro
-----

Members of the Release Team have wanna-build access, and can perform
actions as needed, if appropriate. In particular, the Release Team takes
care of scheduling all binNMUs, and can also perform other actions,
particularly if they are blocking some package from migrating to
testing.

As announced in
<http://lists.debian.org/debian-project/2009/03/msg00096.html>, give-backs
and dep-waits should be requested by sending email to
<debian-wb-team@lists.debian.org>. This includes requests for experimental,
but please mark them as such.

binNMUs should be requested via bugs in the release.debian.org pseudo-package,
with the usertag 'binnmu'. Using reportbug (>= 4.7) partially automates this,
and is recommended. If the binNMUs are required by a transition, please check
if there is an open bug for that transition in the [release.debian.org
pseudopackage][TRANSITIONS], and if so request the binNMUs by following up on
that bug report.

Please note, if you need multiple related actions and at least one of
them is a binNMU, then please file the bug against release.debian.org
and the Release Team will handle it.

[TRANSITIONS]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=transition;ordering=transitions

Syntax
------

Send a mail to the appropriate address with a more or less
descriptive subject, and requests in the format described below. Please
also include in the body any needed explanations, such as "build
dependencies should be installable now", or (always) the reason a
particular binNMU is needed.

Each request goes in its own line, and specifies the type of request, a
list of packages with versions, and a list of arches. All arches apply
to all packages, so if two packages do not exactly share the list of
arches, it's wiser to just give each package its own line.

For dep-waits and binNMUs, a list of packages on which to dep-wait or a
changelog entry, respectively, is needed. These are specified with -m,
please do not forget it.

The syntax is (note the dots, they are required):

    <gb|dw|nmu> PKGS_VER . ARCHES [ [ . SUITE ] . -m 'changelog entry/dep-wait expr.' ]

gb is a "give-back" - a request to set the package's state to *needs-build*,
i.e. to ask the buildds to try building it again, as if it was a new upload.
A give-back does not directly move a package to the needs-build state, it 
first moves to "BD-Uninstallable", where it stays until the wanna-build
system has ensured that build dependencies are installable. This check is
done on a regular basis, several times a day.

dw is a request to set the package's state to *dep-wait*. The string following
'-m' is a dpkg dependency string, such as 'libfoo (>= 1.2.3)'.

See <http://www.debian.org/devel/buildd/wanna-build-states> for descriptions
of the needs-build and dep-wait states.

nmu is a request for a binary NMU, as described in the Debian Developers'
Reference. The string following '-m' is a changelog entry, such as
'recompile against the new libfoo'.

In the list of arches, "ANY" will be expanded to all architectures present in
the suite except the special architecture "all"; see the example below.

Notes
-----

 * If your request applies to a suite other than unstable, the suite should
   be included as the third parameter. For example:
  
     `nmu foo . amd64 . experimental . -m "Rebuild against the new libbar"`

 * If you are asking for a dep-wait, an additional give-back is not
   needed.

 * If your request is for a dep-wait to be cleared, the syntax would be:

     `gb pkg . arch . -o`

 * If you're asking for binNMUs for a transition, you need not use this
   syntax, nor give a comprehensive list of packages: the Release Team
   has tools to auto-generate it, and will do so anyway. Just mention if
   any package should *not* be binNMUed for some reason.


Full example (binNMU)
---------------------

(Use "reportbug release.debian.org" to generate this)

    To: submit@bugs.debian.org
    Subject: nmu: foo_4.3-3

    Package: release.debian.org
    User: release.debian.org@packages.debian.org
    Usertags: binnmu
    Severity: normal

    Hello,

    Due to a mistake, libbar1 was uploaded with a new symbol but
    without bumping the shlibs file.  This is fixed in libbar1 1.2-2,
    but foo on the buildds was built against 1.2-1 except on i386 and
    amd64.

    Please rebuild foo against libbar1 1.2-2 to bump its dependency on
    libbar1.  As libbar1 has not been built yet on mips and mipsel,
    you may want to consider adding a dep-wait.


      nmu foo_4.3-3 . ANY -i386 -amd64 . -m 'Rebuild against libbar1 to correct dependency'
      dw foo_4.3-3 . mips mipsel . -m 'libbar1 (>= 1.2-2)'

Full example (binNMU complex)
-----------------------------

(Use "reportbug release.debian.org" to help generate this)

    To: submit@bugs.debian.org
    Subject: nmu: foo bar, dw foo, gb libfrob

    Package: release.debian.org
    User: release.debian.org@packages.debian.org
    Usertags: binnmu
    Severity: normal

    Hello,

    There was a bug in libfrob (<= 2.1-3) that made packages built
    against it DT_NEED libfrog1 instead of libfrob1. Only foo and bar
    seem affected.  New libfrob is not built everywhere yet, some
    dep-waits are needed.  Also, libfrob's build-dependencies are
    installable on mips now.

      nmu foo_4.3-3 . ANY -i386 . -m 'Rebuild against fixed libfrob, see #111.'
      nmu bar_2:1.0-7 . ANY . -m 'Rebuild against fixed libfrob, see #111.'
      dw foo_4.3-3 bar_2:1.0-7 . amd64 s390x . -m 'libfrob1 (>= 2.1-4)'
      gb libfrob_2.1-4 . mips

    Thanks.

Full example (other wb actions)
-------------------------------

    To: <debian-wb-team@lists.debian.org>
    Subject: Please give back foo on arm*

    Hello,

    There was a bug in libbar1 that caused foo to FTBFS on all arm
    architectures.  This has been fixed in libbar1 1.2-3.  Please give
    back foo on armel, armhf and arm64.

      gb foo_2.1-4 . armel armhf arm64

    Thanks.
