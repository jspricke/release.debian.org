The purpose of this text is to give the release team a few guide lines on
marking bugs etch-ignore. In the end, the decision whether to mark a bug
etch-ignore or not is the decision of the release team member in charge,
i.e. don't argue about what is written here! Also, expect that all of this
will be RC for etch+1.

We ignore, unless fixed fast (i.e. older than 7 days), and unless this is a
regression from sarge (we may extempt from that as well in some cases):
- postrm/purge failures if a dependency is removed;
- obvious license mistakes, as long as the relicensing under a DFSG-license
  is going on with upstream (e.g. upstream forgot to allow distribution of
  modified versions (but has provision for them in the license at another
  place), and agreed to fix that with the next upstream release). This
  doesn't include code that is philosophically non-free, but is meant for
  code that is meant to be free.
- *.pyc in the wrong place
- architecture dependend files in /usr/share
