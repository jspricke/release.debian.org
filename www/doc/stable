Everything you ever wanted to know about Stable Release Management in Debian
                          but were afraid to ask

                                 - or -

          How I Learned to Stop Worrying and Love the Queue Viewer


Introduction
============

Updates to stable
=================

We use distribution tags (e.g. "jessie") to allow us to quickly determine
whether an update bug applies to stable or oldstable.

The workflow for an update is approximately:

- a bug is filed usertagged "pu" (user release.debian.org@packages.debian.org)
  with a minimal debdiff attached and a description of why the update is
  required / desired
- if any changes / more information are required, tag the bug "moreinfo" and
  iterate until a final diff is agreed
- once go ahead is given, tag the bug "confirmed"
- once the upload has occurred and passed initial sanity checks (see "after
  the upload"), tag the bug "pending"
- the package is accepted in to proposed-updates and autobuilt on any
  necessary architectures

Binary rebuilds ("binNMUs") in stable follow a similar workflow, except:

- the bug is usertagged "binnmu"
- the "confirmed" tag is added once the binNMU has been scheduled

A few notes (which could / should be tidied up):

- Added build-dependencies are only appropriate if they are required for
  resolving the issue under discussion.
- Modifying the patch system in use is generally not appropriate for an
  update in stable. This implies that if the package does not currently use
  a patch system then any changes required should be applied directly to the
  upstream source.
- If the issue applies to the version of the package in unstable, then it
  should be fixed there first.
- We encourage the use of the version suffix "+deb${distversion}u${sequence}"
  However, if the uploader wishes to use an
  alternative version which is closer to a "standard" version number then
  this is generally okay, so long as it is verified that there has never
  been an upload to Debian using that version number (this is one reason
  why the version suffixes mentioned are conventional).
- Many updates to stable will be made by people other than the maintainer.
  We do not require the use of NMU version numbers in these cases, nor that
  the changelog mention that it is an NMU.
- There are some classes of bug which we are happy to ignore (including using
  the dist-ignore BTS tags) for stable. These include license clarifications
  where it can be shown that a later correction to the license information
  applies to the stable package; this is particularly the case when the
  corrected information has been included in an upload to unstable.

Interaction with NEW
--------------------

Special care must be taken if an upload to stable will involve the package
traversing the NEW queue.

The archive management software does not support the migration of a package
from one "policy queue" to another. As a result, once a package destined for
stable is accepted from NEW, it will immediately enter proposed-updates,
without ever being in stable-new.

queue-viewer does not auto-generate comments for such uploads, so an
ACCEPTED comment file will need to be manually created; this will then also
trigger the generation of a debdiff. (Unfortunately, this will also be the
first point at which the installability of the package will be checked.)

Removals from stable
====================

Requests for removal from stable should be filed as bugs against release.d.o,
usertagged "rm" (user release.debian.org@packages.debian.org) with an
appropriate distribution tag applied. If removal of the package from both
stable and oldstable (when possible) is requested, this should be tracked
using two separate bugs.

Assuming the removal is agreed, the bug should be tagged "pending" and
retitled to follow ftp-master's standard form for removal bugs (almost always
using "RoM" or "RoQA", depending on who requested the removal). At this
point an entry should also be added to the special "REMOVALS" comment file, in
the format:

  #${bugnumber}: ${source} - ${reason}  

If an update for the package has been previously issued via the security
archive, those packages should also be removed, via a bug against the
ftp.debian.org psuedopackage. The report should note that the removal of the
package from stable is planned to be actioned during the point release.

After the upload
================

A mail with the diff is sent by queue viewer to stable-diffs@, so that
it can be checked. One way to do so is running interdiff between what got
reviewed (diff in BTS) and what got uploaded (diff in mail).

Processing is done by creating "comments" files. Those live on coccia under:
  /srv/release.debian.org/www/proposed-updates/stable_comments
  /srv/release.debian.org/www/proposed-updates/oldstable_comments

(A symlink from ~ to those helps.)

The filename format is $source_$version. For an accept, the first two lines
must be of the form:

OK
$source - $reasons

$reasons for this upload is a short summary of bug fixes, CVE IDs, etc.

If the final line of the file is of the form "Bug#NNNNNN", where NNNNNN
is the number of the BTS request for the update, then the bug will
automatically be tagged "pending" as part of accepting the upload. In
order to stop that text being shown by queue-viewer, one can prefix it
with a standard format signature separator. Thus, a complete example
file might be:

OK
$source - $reasons
-- 
Bug#NNNNNN

The "pending" mail will be sent using your shell account on coccia, so you
may want to set $DEBEMAIL (and possibly $DEBFULLNAME) in your environment.

Once it's done, make sure the queue viewer is happy:
  https://release.debian.org/proposed-updates/oldstable.html
  https://release.debian.org/proposed-updates/stable.html

If it is, ask dak to accept the package by running
  /srv/release.debian.org/tools/scripts/srm \
    accept pu $comment_filename

and supply your "sudo" password when prompted.

This will create an "ACCEPT" comment in ftp-master's queue, which will
renamed to ACCEPTED.source_version when ftp-masters have
taken it into account. A "NEW changes in stable-new" (or oldstable-new)
mail is sent to debian-release@ in that case.

[Note that historically the comments directories lived within ftp-master's
queues and were directly manipulated there. In order to separate the
ftp-master and release services, the ftp-master directories are now
only manipulating using SSH triggers, wrapped by the "srm"
script detailed above. A second script, run from release's crontab directly
prior to queue-viewer itself, ensures that any changes to the ftp-master
directories are reflected in queue-viewer's comment files.]

If a pu bug was filed (see "BTS handling") then it should be tagged
"pending" at this point.

If for some reason you need to reject a package, modify the first line
of the comment file to be "NOTOK" and, as with accepts, signal dak to
action the comment by:

  /srv/release.debian.org/tools/scripts/srm reject pu \
    $comment_filename

to create the file in the ftp-master queue, which will be
renamed to REJECTED.source_version once processed and the actions taken
included in a mail to debian-release.

The reasons listed in the comment file will be used as part of the reject
message sent by dak.

Once the rejection has occurred, if a new upload of the same version
is expected, you may wish to tidy up some metadata so that it gets
re-generated for the new version:

- mv stable_comments/REJECTED.$foo stable_comments/cruft
- remove or rename ~release/www/proposed-updates/stable_diffs/${foo}.debdiff
- remove ~release/www/proposed-updates/stable_debcheck/${foo}_*.debcheck

Security updates
----------------

As part of the release of a DSA, the updated packages are pushed from
security-master to ftp-master where, all things being well, dak will
copy them to p-u-NEW.

When looking at packages in the -NEW queue for which there is not yet
a comment file, queue-viewer will compare the package name and version
to those in DSA/list (from the secure-testing repository).  If a match
is found, it will automatically create a comment file, with status
"UNKNOWN".  After a quick manual check, this can be updated to "OK".

If for some reason the information in the DSA list is missing (and this
can't / won't be fixed) then you can manually create a comment file in
the same way as above, but using the format below for the second line.

DSA 1234 $source - $reasons

This generally only needs doing when -2 DSAs are published, or a DSA
is released for an issue which is not actually a security issue in the
updated package itself - for instance, to resolve a regression introduced
by a security update of another package.  In these cases, the updated
package information is often not recorded in DSA/list.

The queue status of a security upload will only change to "OK" with the
green background once packages are available for all of the architectures
on which the package is built in the base suite.  Until this point, it
will remain "in limbo", with the missing architectures highlighted.

_DO NOT_ accept a package that is "in limbo".  This will cause the buildd
network to believe that the remaining architectures need to be built in
stable and can lead to the main archive and the security archive serving
packages with the same name/version but different content.

debian-installer
----------------

The installer is a little special, as it is auto-BYHANDed (don't blame me,
I didn't invent the name) by dak.  As a side-effect, the images are installed
directly into dists/proposed-updates.  The packages are then accepted using
comments files as usual.

If there are no changes to the debian-installer source package itself, it
can be binNMUed in order to incorporate changes from other packages.  (An
exception to this is the first debian-installer build for a given release.
This must be a source upload, in order to enable the "build against
proposed-updates" switch in the code.)

Once the installer is built on all architectures, there will need to be an
upload of debian-installer-netboot-images.

stable-updates
==============

Sometimes an update is sufficiently important that it merits release to a
wider audience before the next point release.  This is handled via the
"stable-updates" suites.

Releasing a package via stable-updates first requires accepting it into
proposed-updates from the -NEW queue.  In most cases you will also want to
wait for the package to be built on all architectures before releasing it.
Note that missing architectures will not be automatically included, so you
will have to take care to add any that are not included in the initial
release.

Similarly to testing, the Release Team have the ability to "set" the current
content of stable-updates.  Start by making a copy of
/srv/release.debian.org/sets/jessie-updates/current to a datetime-stamped
file in the same directory.  Add the source and binaries which form part of
the update, in the same style as the existing entries (as returned by
"dak control-suite" or "dak ls -f control-suite").  If the package has
previously been released via stable-updates, the entries for the old
version should be removed.
(hint: dak ls -f control-suite -s jessie-proposed-updates -S <pkg>)

Once you're ready to proceed with the release, symlink the new file to
"current" and, as the release user:

md5=$(md5sum /srv/release.debian.org/sets/jessie-updates/current | awk '{print $1}')
ssh -2 -T -o BatchMode=yes -i ../ssh/jessie-updates_trigger_key dak@fasolo.debian.org $md5 < \
  /srv/release.debian.org/sets/jessie-updates/current

This will update the suite on ftp-master, ready to be pushed to mirrors at
the next dinstall.

An announcement mail will also need to be prepared, and sent to
debian-stable-announce@lists.d.o.  Although it is not required to
GPG sign mail to that list, it is conventional to do so.  The list is
moderated, so you may need to poke one of the moderators* if your mail
does not appear on the list after a while.  It is preferred to have the
mail appear on the list before the dinstall in which the package is
released, so that users are not surprised by the availabilty of a new
package.

Hints:
 + doc/templates/stable-updates-mail.example is an example mail
   (which needs adjusting to suit)
 + The name at the top of the announcement should be the package author,
   not you

* As of 2014-03-09, the moderators are adsb, pkern, jcristau and kibi.

Point releases
==============

Before
------

Once p-u-NEW is frozen
~~~~~~~~~~~~~~~~~~~~~~

Create /srv/release.debian.org/www/${codename}/${version}/${pointversion}/;
for example "/srv/release.debian.org/www/jessie/8/8.3/.

Generate an "upcoming point release" accouncement mail, and send it to
debian-stable-announce@lists.debian.org.  Although it is not required to
GPG sign mail to that list, it is conventional to do so.  The list is
moderated, so you may need to poke one of the moderators if your mail
does not appear on the list after a while.

~release/tools/scripts/point-release-mail -d -s stable -t ~release/tools/scripts/TEMPLATE.p-u-frozen

The -d flag indicates that a draft mail should be generated.  This is often
useful at this stage of the process, as it includes packages that are not
completely ready (although you will have to move them from the "needs
investigation" section to wherever they should be and then drop that
section and the other indications of it being a draft, before sending).

Prepare some sets of data which will be useful during the point release
- some for the release team, some for ftp-master.  Note that some of
these steps may be skipped if they do not apply to a particular point
release; filenames may need to be updated as appropriate.

The debian-debug archive (comprising automatically generated -dbgsym
packages containing debugging symbols) was first introduced with the
stretch release, so any items below related to it should be ignored
for earlier releases.

State of stable at various stages of the point release (expected)
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

In order to avoid issues with differing locales amongst users involved
in a point release, all of the below should be run with "LC_ALL=C" in
force (either globally or applied to all invocations of "sort" and
"comm").

comments=/srv/release.debian.org/www/proposed-updates/stable_comments
cd /srv/release.debian.org/www/jessie/8/8.3

mkdir before
cp -a /srv/ftp.debian.org/mirror/ftp-master/dists/stable/ before
cp -a /srv/ftp.debian.org/mirror/ftp-master/dists/stable-debug/ before
rm -rf before/stable/main/installer-*

zcat before/stable/main/source/Sources.gz > \
  Sources-before
zcat before/stable-debug/main/source/Sources.gz > \
  Sources-debug-before
# Architectures may be changed
zcat before/stable/main/binary-amd64/Packages.gz > \
  Packages-amd64-before
zcat before/stable-debug/main/binary-amd64/Packages.gz > \
  Packages-debug-amd64-before
zcat before/stable/Contents-powerpc.gz > \
  Contents-powerpc-before
zcat before/stable-debug/Contents-powerpc.gz > \
  Contents-debug-powerpc-before

# Mostly useful for preparing other files
dak control-suite -l stable | sort > stable-before.cs
dak control-suite -l stable-debug | sort > stable-debug-before.cs
dak control-suite -l proposed-updates | sort > \
  pu-before.cs
dak control-suite -l proposed-updates-debug | sort > \
  pu-debug-before.cs
# Initial attempt at "new stable"
cat stable-before.cs pu-before.cs | sort > combined.cs
cat stable-debug-before.cs pu-debug-before.cs | sort > \
  combined-debug.cs
# Generate a list of removals
dak ls -S -s stable -f control-suite \
  $(awk '{print $2}' ${comments}/REMOVALS) \
  | sort > removals.cs
dak ls -S -s stable-debug -f control-suite \
  $(awk '{print $2}' ${comments}/REMOVALS) \
  | sort > removals-debug.cs
# Apply the removals
comm -3 combined.cs removals.cs > combined-after-removals.cs
comm -3 combined-debug.cs removals-debug.cs > \
  combined-debug-after-removals.cs
# "Dominate" new stable
/srv/release.debian.org/tools/scripts/naive-heidi-dominate \
  < combined-after-removals.cs | sort > combined-dominated.cs
/srv/release.debian.org/tools/scripts/naive-heidi-dominate \
  < combined-debug-after-removals.cs | sort > combined-debug-dominated.cs
# Quick visual check of the results of domination
diff -adNru combined.cs combined-dominated.cs
diff -adNru combined-debug.cs combined-debug-dominated.cs
# In the absence of cruft, removals or new packages, these should have
# the same number of packages in
wc -l stable-before.cs combined-dominated.cs
wc -l stable-debug-before.cs combinated-debug-dominated.cs

Check the output of "dak cruft-report -s stable" and for stable-debug.
 If there's anything relevant, note it in to TODO and consider adding a
new cs file.

Source compliance (mostly d-i initrd contents)
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

If there is a new debian-installer included in the point release, the
source and binary packages need to be added to the -r0 suite.

The debian-installer build produces a Built-Using field, so dak will
ensure that any other required packages are kept in the archive; it
is no longer necessary (nor desirable) to manually maintain a list of
such packages, so running do-stable-source-tracking is no longer
required.

Packages requiring propogation to testing and/or unstable
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

for suite in testing unstable
do
  suitecomp $pusuite gt $suite > propup.$suite
done

$pusuite is either "proposed-updates" or "oldstable-proposed-updates".

If propup.unstable is non-empty, add a note (including the filename)
to TODO.  The prop-ups to testing can be done by the release team,
generally between the end of the point release and the following britney
run.

queue-viewer
=-=-=-=-=-=-

Once all builds for all packages are available:

- copy /srv/release.d.o/www/proposed-updates/stable* and
  /srv/release.d.o/www/proposed-updates/stable_comments/
  to /srv/release.d.o/www/jessie/8/8.3
  
At some point between here and the point relesae, de-cron updates
to queue-viewer.


T-3 days (so generally Wednesday)
~~~~~~~~

Draft a full announcement, commit it to the publicity-announcements repository
(git+ssh://$USER@git.debian.org/git/publicity/announcements.git/) and send 
a mail to press@debian.org and debian-publicity@lists.debian.org

~release/tools/scripts/point-release-mail -s stable -t ~release/tools/scripts/TEMPLATE.wml

In some cases the dates for DSAs won't be automatically located and will be
listed as XXXX; these will need to be fixed by hand.  The auto-generated
package comments should be reviewed and tweaked as necessary.

If there's any particularly interesting kernel or installer changes included
in the point release, you may wish to add a section to the mail about these.

In case any further packages are accepted after this point, send a diff for
the text to the press team.


Special comments files
  - TODO
  - Removals

Tools
  - point-release-mail

Preparation

During
------

ftp-master will begin by disabling their crontab and making a backup of the
projectb database.

If there are any packages in p-u which should not be released during this
point release, make sure you tell them early in the process or they may
start proceeding on the basis that all packages should be included.

You may find it helpful to mention to $ftp-master-of-the-day at the start
of the process whether there are any removals, prop-ups, jessie-r0 updates,
d-i updates, etc. to be covered during the point release.  They should ask
about each of these things as they run through their own checklist, but
this will allow them to know they can skip steps if there are no removals,
for example.

The ftp-master will often mention what they're doing as the point release
progresses.  You can follow much of what's going on by observing
/srv/ftp-master.debian.org/log/current on coccia and the output of "dak ls" and
"dak control-suite" at various points.  Where you have previously prepared
"before" or "expected" files, try and capture a matching set of "after" or
"actual" data for comparison purposes.  If there are any differences between
the expected and actual data, reconcile these, prompting ftp-master if
necessary.

Once any packages have been copied between suites and removed and any
required changes to the set of installer versions for stable have been made,
ftp-master will request that a check of the new state be made; domination
of stable may or may not have occurred at this point.

Assuming everything is okay, the new filesystem metadata files will start
being generated.  This will include packages, sources, contents and release
files.

ftp-master will again ask for a check, followed by a signature of
the top-level release file(s), using the stable release key.

Things to check include:

- ftp/README should have the correct version number and release date.
- ftp/README.html should have the correct version number, release date and
  "Modified" date.
- ftp/dists/README should have the correct version number.
- ftp/dists/stable/Changelog show contain:
  - a header with the correct version number and release date.
  - a list of any removals.
  - the contents of the .changes from any included uploads.
- the InRelease file(s) should be deleted.
- pick some random entries from the top-level Release file(s) and verify that
  the checksums listed match the corresponding files.

For each of the packages / sources / contents files which were prepared
before the point release, check a diff between the original and new files.

Once everything appears satisfactory, sign the Release file(s) and let
ftp-master know where the signature is.

After
-----

Mail debian-security@lists.debian.org (as people have in the past tended to
panic slightly when updated packages appear without an annoucement) and the
press team, to let them know that the point releases has finished.  This
should include whether the packages are being pushed out straight away or
waiting for a scheduled dinstall (and when that is).

Create stable_comments/cruft/<longversion> and move stable_comments/ACCEPTED.* in to it.
Also copy stable_comments/{REMOVALS,TODO} to the new directory and then empty the
live versions.  Re-enable the queue-viewer cron job.

Close each of the currently pending release.d.o bugs corresponding to the
just released packages, using <longversion> as the "fixed" version.

Keep an eye out for any issues being mentioned on IRC or on -release /
-security.

Updates to SRM tools after a Debian release
===========================================

queue-viewer configuration [in release.d.o git/etc]
  - update architecture lists for stable and oldstable
  - re-enable oldstable
  - create ~release/www/proposed-updates/${codename}_{diffs,debcheck}
  - move oldstable_{diffs,debcheck} symlinks to point to the now-oldstable
  - move stable_{diffs,debcheck} symlinks to point to ${codename}_{diffs,debcheck}

secconnect/update-dsadb
  - update suite list

crontab
  - re-enable stable-compare for oldstable

tools/config.ini
  - update release_architectures for testing

tools/scripts/config.ini
  - update names / versions for stable / oldstable
  - update architecture lists for p-u / o-p-u
                    
BTS
  - update usertags used for pu, opu and stable / oldstable rm bugs

If any packages were moved from t-p-u to p-u during the release, queue-viewer
will not see them until manual ACCEPTED comments files are created for them.
