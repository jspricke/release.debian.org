#! TITLE: Debian Release Management
#! SUBTITLE: quando paratus est

# Documentation

 * [hints](../../doc/britney/hints.html)

# User hints files

 * [aba](aba)
 * [adsb](adsb)
 * [elbrus](elbrus)
 * [faw](faw)
 * [ivodd](ivodd)
 * [jcristau](jcristau)
 * [jmw](jmw)
 * [kibi](kibi)
 * [mehdi](mehdi)
 * [nthykier](nthykier)
 * [pkern](pkern)
 * [pochu](pochu)

# Role hint files

 * [auto-removals](auto-removals)
 * [freeze](freeze)
 * [freeze-exception](freeze-exception)

# Other relevant control files

 * [transitions.yaml](https://ftp-master.debian.org/transitions.yaml)

