NULL =
BRITNEY2_SRC_DIR = britney/code/b2
BRITNEY2_WWW_DIR = www/doc/britney
OUTPUT_DIRS =$(BRITNEY2_WWW_DIR) \
	     $(NULL)
OUTPUT_FILES=www/testing/arch_qualify.html \
	     www/testing/freeze_policy.html \
	     www/doc/index.html \
	     www/wanna-build.html \
	     britney/hints/index.html \
	     $(NULL)

all: $(OUTPUT_FILES) britney-docs

clean:
	rm -f $(OUTPUT_FILES)
	rm -fr $(OUTPUT_DIRS)
	$(MAKE) -C $(BRITNEY2_SRC_DIR) clean

britney-docs:
	$(MAKE) -C $(BRITNEY2_SRC_DIR) docs
	rsync -a $(BRITNEY2_SRC_DIR)/_build/html/ $(BRITNEY2_WWW_DIR)/

%.html: %.md markdown-templates/header.html markdown-templates/footer.html markdown-templates/markdown-wrapper
	markdown-templates/markdown-wrapper markdown-templates/header.html < $< | cat - markdown-templates/footer.html > $@.tmp
	mv -f $@.tmp $@

%/arch_qualify.html: %/arch_qualify.py %/arch_spec.yaml %/waivers_spec.yaml %/arch_qualify_header.html %/arch_qualify_footer.html
	cat $*/arch_qualify_header.html > $@.tmp
	python $*/arch_qualify.py $*/arch_spec.yaml $*/waivers_spec.yaml >> $@.tmp
	cat $*/arch_qualify_footer.html >> $@.tmp
	mv -f $@.tmp $@
